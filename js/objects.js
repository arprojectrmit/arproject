//This is where we store all the objects
var objects = new Array();
var objectss = ["hello"];


//To be implemented as an array later, to support multiple markers
var markerIDs = [22];

// Variables for object animations.
var deltaVal;


function generate_id()
{
	var ID = 0;

	//Get the highest ID number in objects so far
	for(var i = 0; i < objects.length; i++)
	{
		if(objects[i].ID > ID)
		ID = objects[i].ID;
	}
	
	ID++;

	return ID;
}


function add_cap_arm(name) 
{

	var cap_arm = create_joint('cap_arm', 0, 0, 0);

	var wheel = add_cylinder( 30, 30, 5, 16);
	var wheel_joint = create_joint('wheel_joint', 0, 2.5, 0);
	var arm = add_cube( 12, 120, 12 );
	var grabber_housing = add_cube( 5, 5, 20 );
	var grabber_housing_joint = create_joint('grabber_housing_joint', 0, 55, -6);
	var grabber_joint = create_joint('grabber_joint', 0, -2.5, -5);
	var grabber = add_cylinder( 2.2, 2.2, 20 );

	// Unusual indentation is to visualise heirachy of models various objects.
	// and their joins.
	
	cap_arm.add(wheel);
		wheel.add(wheel_joint);
			wheel_joint.add(arm);
				arm.add(grabber_housing_joint);
					grabber_housing_joint.add(grabber_housing);
						grabber_housing.add(grabber_joint);
							grabber_joint.add(grabber);


	// Init position settings for various parts of cap_arm.
	wheel.rotation.x = Math.PI / 2;
	wheel_joint.rotation.x = -Math.PI / 2;
	arm.position.set(0, 60, -15);
	grabber_housing.position.set(0, 0, -10);
	grabber_housing.rotation.z = Math.PI / -9;
	grabber.position.set(0, -7.5, 0);


	// Any object can have a 'animation' variable. To make an object animateable just
	// copy this function call but replace 'cap_arm' to the three.js object you are
	// creating, eg 'cube' or 'cylinder' if you were adding this to those generic functions
	// below. Then change 'grabber' to whatever part of the object you wish to animate.
	// In objects consisting of only itself then it would be just itself, eg 'cube' or 'cylinder'
	// again. In this case grabber is one object of cap_arm's many objects. So by selecting that
	// we only animate that part rather then the whole thing.
	cap_arm.animation = function(name, dt) {

		// Call to animate selected object. Can animate object's position or rotation but must also 
		// select a specific axis eg, 'x', 'y' or 'z'. See animate_obj function for explanation
		// of it's parameters. 
		wheel.rotation.y = 					animate_obj(name, dt, 8, [0, 0.2, 0.3, 0.375, 0.7, 0.8, 0.875, 1], [0, 1.3, 1.3, 1.1, -1.2, -1.2, -1, 0]);
		grabber_housing_joint.rotation.z = 	animate_obj(name, dt, 8, [0, 0.2, 0.3, 0.375, 0.7, 0.8, 0.875, 1], [0, -1.3, -1.3, -1.1, 1.2, 1.2, 1, 0]);
	}
	
	add(name, cap_arm, markerIDs);

	return cap_arm;
}

// ===========================================
// Generic three.js object building functions.
// ===========================================

// Simple cube with setable length, width, depth parameters.
function add_cube(length, width, depth)
{
	var geometry = new THREE.BoxGeometry( length, width, depth);
	var material = new THREE.MeshNormalMaterial();
	var cube = new THREE.Mesh( geometry, material );

	material.side = THREE.BackSide;

	//console.log(cube);
	//console.log(geometry);

	//add(name, cube, markerIDs);
	
	return cube;
}

// Simple cylinder with setable length, width, depth parameters.
function add_cylinder(diameter_top, diameter_bottom, height, segments) {
	var geometry = new THREE.CylinderGeometry(diameter_top, diameter_bottom, height, segments);
	var material = new THREE.MeshNormalMaterial();
	var cylinder = new THREE.Mesh( geometry, material );

	material.side = THREE.BackSide;

	//add(name, cylinder, markerIDs);
	
	return cylinder;
}

//Function used to create various three.js joints.
function create_joint(name, x, y, z) 
{
	var joint = new THREE.Object3D();
	joint.name = name;
	joint.position.x = x;
	joint.position.y = y;
	joint.position.z = z;

	return joint;
}


function add_conveyer(name)
{
	var geometry = new THREE.PlaneGeometry(200,40);
	var material = new THREE.MeshNormalMaterial();
	material.transparent = true;
	material.side = THREE.BackSide;
	material.opacity = 0.5;
	var conveyer = new THREE.Mesh( geometry, material );
	
	//Only need to add to object array if clickable object
	add(name, conveyer, markerIDs);
	
	return conveyer;
}

function add_plane(name)
{
	var geometry = new THREE.PlaneGeometry(100,100);
	var material = new THREE.MeshNormalMaterial();
	var plane = new THREE.Mesh( geometry, material );

	add(name, plane, markerIDs);
	
	return plane;
}

function add_sphere(name)
{
	var material = new THREE.MeshNormalMaterial();
	var sphereGeometry = new THREE.SphereGeometry(30, 32, 16);
	var sphere = new THREE.Mesh( sphereGeometry, material );
	sphere.position.set(10,10,10);

	add(name, sphere, markerIDs);
	
	return sphere;
}

function add_cap(name)
{
	var geometry = new THREE.CylinderGeometry( 10, 10, 20, 32 );
	var material = new THREE.MeshNormalMaterial();
	material.side = THREE.BackSide;
	var cylinder = new THREE.Mesh( geometry, material );
	
	cylinder.geometry = geometry;

	add(name, cylinder, markerIDs);
	
	return cylinder;
}

function add_model(name, model_directory)
{
	//The bottle model
	new THREE.JSONLoader().load(model_directory, function(geometry)
	{
		var material	= new THREE.MeshNormalMaterial();
		var model	= new THREE.Mesh(geometry, material);
		
		/*bottle.position.set(-70, -100, -10);
		bottle.scale.set(5,5,5);
		//Need to rotate 90 degrees, as marker as vertical
		bottle.rotation.x = -Math.PI / 2;*/
		
		//Add to objects array, generating outline and textbox, initialise click value etc
		//add(ID, obj, geometry, markerId)
		add(name, model, markerIDs);		
	});
	
	return model;
}

function add_json(name, json_string)
{

	/*var loader = new THREE.JSONLoader();

	
	console.log(json_string);
	var model = loader.parse( json_string );

	var mesh = new THREE.Mesh( model.geometry, model.materials[ 0 ] );*/
	var loader = new THREE.OBJLoader();
	
	var obj = loader.parse(json_string);
	
	var mesh = obj.children[0];
	
	mesh.material = new THREE.MeshNormalMaterial();

	add(name, mesh, markerIDs);
}

function add_load(name, json_string)
{

	var loader = new THREE.JSONLoader();

	
	console.log(json_string);
	var model = loader.parse( json_string );

	var mesh = new THREE.Mesh( model.geometry, model.materials[ 0 ] );

	add(name, mesh, markerIDs);
}


//Outline for highlighting clicked object
function genOutline(obj)
{
	var outlineMaterial = new THREE.MeshBasicMaterial( { color: 0xff0000, side: THREE.BackSide, transparent: true} );
	//Setting the material to tranparent, as I could not get it to sit behind the object.
	outlineMaterial.opacity = 0.25;
	var outlineMesh = new THREE.Mesh( obj.geometry, outlineMaterial );
	outlineMesh.position.set(obj.position.x, obj.position.y, obj.position.z) ;
	outlineMesh.scale.set(obj.scale.x, obj.scale.y, obj.scale.z);
	outlineMesh.rotation.x = obj.rotation.x;
	outlineMesh.scale.multiplyScalar(1.1);
	
	return outlineMesh;
};

//Textbox for objects
function genTextbox()
{
	var textbox = document.createElement('div');
	textbox.style.position = 'absolute';
	textbox.style.width = 100;
	textbox.style.height = 100;
	textbox.style.backgroundColor = "white";
	textbox.style.border= "black";
	textbox.innerHTML = "Default message.";
	textbox.style.visibility = "hidden";
	container.appendChild(textbox);
	return textbox;
}


//Checks for an object with given ID, and returns its index in the array if found
function containsObject(ID, list) 
{
   	//Check if an object with this ID has been added
	for(var i = 0; i < list.length; i++ )
	{
		if(list[i].ID == ID)
		{
			return i;
		}
	}
	
	return -1;
}

//Add to object array with relevant attributes
function add(name, obj, markerIDs)
{
	//Check if an object with this ID has been added
	//var index = containsObject(ID, objects);
	
	var exist = (name in objects);
	//If false, add the object
	if(!exist)
	{
		objects[name] = new Array();
		//Give the object an id - must be unique, or object will be overwritten;
		//obj.ID = ID;
		objects[name]["name"] = name;
		//The "clicked" tag means this object should have a text box and be highlighted
		objects[name]["clicked"] = false;
		//save the geometry for means of drawin outline later (there may be another way to access geometry?)
		// obj.geometry = geometry;			// I believe this is redundant, obj already comes with the geometry. - Phil
		//generate outline
		if(obj.geometry != null)
		{
			objects[name]["outline"] = genOutline(obj);
			objects[name]["outlineAdded"] = false;
		}
		if(obj.animation != null)
		{
			console.log("set animation for " + name);
			objects[name]["animation"] = obj.animation;
			objects[name]["currentAnimTime"] = 0;
		}
		//save a textbox to this object
		objects[name]["textbox"] = genTextbox();
		//And save the marker for this instance of the object
		objects[name]["markerIDs"] = markerIDs;
		//For animating
		objects[name]["interval"] = null;
		//Add object to object array to test for clicks
		objects[name]["object"] = obj;
		//console.log(obj);
		
		console.log("added " + name);

	}
	//Else copy previous data, essentially updating the obj. This might be better doen with an object node, with the object as a variable
	else
	{
		console.log("object already exists with name: " + name);
	}
}

// Returns an objects animation movement value at a certain point in time. This is where the interpolation's
// magic happens. The parameters allow for good control on individual animations.
// 			- Dt is the time since last animation cycle.
//			- totalAnimTime is the full duration of the animation in seconds.
//			- keys is an array of various points in time that are of interest. It always begins
//			  with '0' and finishes with '1'.
//			- values is an array of translation values for the animation. These are the values that
// 			  will be applied to the object. Their application to the object coincides with the keys
//			  array so they must have the same number of elements. Think of it like this... 
//			  "At 'keys' position X is the point in time' at which the object moves by 'values' position X."
// For an example see function add_cap_arm().
function animate_obj(name, dt, totalAnimTime, keys, values) {

	
	var object = objects[name]["object"];
	var stats = objects[name];
	
	if(debug)
	{
		console.log("running animation for " + name);
		console.log("animation time:" + stats["currentAnimTime"]);
		console.log("dt: " + dt);	
	}

	// Marks what positions from the animation's keys 
	// are currently being observed.
	var firstPos = 0;
	var secondPos = 1;

	var deltaKey;
	var normalKeyTime;
	var normalisedTime = 0;

	//Divide 'dt' by say 6 will put the animations in slow-mo. 
	stats["currentAnimTime"] += dt;

	normalisedTime = stats["currentAnimTime"]/totalAnimTime;

	while (secondPos < values.length) {
		if (keys[firstPos] <= normalisedTime && keys[secondPos] >= normalisedTime) {
			break;
		} 
		else {
			firstPos++
			secondPos++
		}
	}

	deltaVal = values[secondPos] - values[firstPos];
	deltaKey = keys[secondPos] - keys[firstPos];
	normalKeyTime = normalisedTime - keys[firstPos];
    normalKeyTime = normalKeyTime / deltaKey

    result = normalKeyTime * deltaVal + values[firstPos];
	
		//Starts animations again once it has run all the way through.
	if (stats["currentAnimTime"] >= totalAnimTime) 
	{
		if(debug)
		console.log("animation end");
	
		stats["currentAnimTime"] = 0;
		clearInterval(stats["interval"]);
		result = values[firstPos];
		return result;
	}

	return result;
}






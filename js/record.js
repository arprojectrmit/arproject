var screenshot = 0;


function dataURLtoBlob(dataurl) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], {type:mime});
}

function PNGSequence( canvas ){
  this.canvas = canvas;
  this.sequence = [];
};
PNGSequence.prototype.capture = function( fps ){
  var cap = this;
  this.sequence.length=0;
  this.timer = setInterval(function(){
    cap.sequence.push( cap.canvas.toDataURL() );
	var img = new Image();
	img.src = cap.canvas.toDataURL();
	document.body.appendChild(img);
  },1000/fps);
};
PNGSequence.prototype.stop = function(){
  if (this.timer) clearInterval(this.timer);
  delete this.timer;
  return this.sequence;
};

var myCanvas = document.getElementById('renderer');
var recorder = new PNGSequence( myCanvas );

var newButton = document.createElement('input');
newButton.type = 'button';
newButton.value = 'record (5 seconds)';
newButton.id = 'record';
newButton.onclick = function () 
{
	recorder.capture(1);
};
document.getElementById('header').appendChild(newButton);

var newButton = document.createElement('input');
newButton.type = 'button';
newButton.value = 'snapshot';
newButton.id = 'shot';
newButton.onclick = function () 
{
	var img = new Image();
	img.src = document.getElementById('c').toDataURL();
	document.body.appendChild(img);
};
document.getElementById('header').appendChild(newButton);

var newButton = document.createElement('input');
newButton.type = 'button';
newButton.value = 'save';
newButton.id = 'save';
newButton.onclick = function () 
{
	var newWindow = window.open(recorder);
};
document.getElementById('header').appendChild(newButton);


// Record 5 seconds
setTimeout(function(){
  var thePNGDataURLs = recorder.stop();
}, 5000 );

var data2 = [false, 1, 2, 3, 'four', { five: 6 }];

var save_dir = document.createElement("INPUT");
save_dir.setAttribute("id", "save_dir")
save_dir.placeholder = "Save directory:";
document.getElementById('save_objects').appendChild(save_dir);

var newButton = document.createElement('input');
newButton.type = 'button';
newButton.value = 'save objects';
newButton.onclick = function () 
{
	var exporter = new THREE.OBJExporter();
	var i = 0;
	var saved_objects = new Array();
	
	var name;
	var geometry;
	var position;
	var type;
	
					
	for(object in objects)
	{
		var cur_object = objects[object];
		var model = cur_object["object"];
		name = cur_object["name"];
		type = cur_object["type"];
		geometry = exporter.parse( model);
		position = model.position.x + "," + model.position.y + "," + model.position.z;
		//position = JSON.stringify(preposition);
		console.log(position);
		

		$.ajax({
					type: 'post',
					url: './js/save.php',
					data: 
					{
						name: name,
						type: type,
						//geometry: geometry,
						position: position,
						save_dir: $('#save_dir').val()
					},
					success: function( data ) {
						
						//console.log( data );
					}
				});
	}
}
document.getElementById('save_objects').appendChild(newButton);


var load_dir = document.createElement("INPUT");
load_dir.setAttribute("id", "load_dir")
load_dir.placeholder = "load directory:";
document.getElementById('save_objects').appendChild(load_dir);

var newButton = document.createElement('input');
newButton.type = 'button';
newButton.value = 'load objects';
newButton.onclick = function () 
{
	var string;
	var object_string;

	function load_array()
	{
		return $.ajax({
		type:"POST",
		url:"./js/load.php",
		data: 
		{
			load_dir: $('#load_dir').val()
		},
		success:function(msg){
			console.log(msg);	
			//string =  JSON.parse(msg);
			object_string = msg;
		}
		});
	}
	
	load_array().done(function(r) 
	{
		
		var objects_split = object_string.split("#");
		
		var numargs = 3;
		
		for(var j = 0; j < objects_split.length; j++)
		{
			console.log("Object:" + objects_split[j]);
			var string = objects_split[j];
			var string_split = string.split("@");
			for(var i = 0; i < string_split.length; i+=numargs)
			{
				var name = string_split[i];
				name = name.replace(/["']/g, "");
				console.log(name);
				var type = string_split[i+1];
				var pos = string_split[i+2];
				console.log("pos:" + pos);
				var pos_values = pos.split(",");
				
				var event = {event_type: "create", name: name, object_type: type};
				
				handle_event(event);
				
				objects[name]["object"].position.set(parseInt(pos_values[0]), parseInt(pos_values[1]), parseInt(pos_values[2]));
				
				console.log(objects);
			}
		}
		
	})
	.fail(function(x) 
	{
		// Tell the user something bad happened
	});	
}
document.getElementById('save_objects').appendChild(newButton);

var newButton = document.createElement('input');
newButton.type = 'button';
newButton.value = 'show objects in console';
newButton.onclick = function ()
{
	console.log(objects);
} 
document.getElementById('save_objects').appendChild(newButton);
//onDocumentMouseDown() uses the following variables. We'll store all of our "clickable" objects in objects array. 
var mouse = new THREE.Vector2();
var raycaster = new THREE.Raycaster();
document.addEventListener( 'mousedown', onDocumentMouseDown, false );

//Anything concerning clickable objects goes here
function onDocumentMouseDown( event ) 
{
	//event.preventDefault();
	mouse.x = ( event.clientX / renderer.domElement.clientWidth ) * 2 - 1;
	mouse.y = - ( event.clientY / renderer.domElement.clientHeight ) * 2 + 1;

	raycaster.setFromCamera( mouse, camera );
	
	console.log("click detected")
	
	for (var key in objects) 
	{
		//console.log(objects[key]);
	}
	
	for (var key in objects) 
	{
		var intersect = raycaster.intersectObject( objects[key]["object"], true );
		console.log(intersect);
	
		if(intersect.object != null)
		{
			console.log("click object " + objects[key].name);
			var clicked_object = intersect.object;

			var markerx = markers[clicked_object.markerIDs[0]];
			
			if(clicked_object.clicked == false)
			{
				clicked_object.clicked = true;
				
				if(clicked_object.outlineAdded == false)
				{
					markerx.object3d.add( objects[i].outline);
					clicked_object.outlineAdded = true;
				}

				clicked_object.textbox.style.visibility = "visible";
				
			}
			else
			{
				clicked_object.clicked = false;
				if(clicked_object.outlineAdded == true)
				{
					markerx.object3d.remove(objects[i].outline);
					clicked_object.outlineAdded = false;
				}
				
				clicked_object.textbox.style.visibility = "hidden";
			}
		}
	}
}
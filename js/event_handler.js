var clock = new THREE.Clock();

var functions = new Array();

functions["cap"] = function (name) { add_cap(name);};
functions["cap_arm"] = function (name) { add_cap_arm(name);};
functions["sphere"] = function (name) { add_sphere(name);};
functions["plane"] = function (name) { add_plane(name);};
functions["conveyer"] = function (name) { add_conveyer(name);};

var obj_name = document.createElement("INPUT");
obj_name.placeholder = "Object Name";
document.getElementById('create_objects').appendChild(obj_name);

var obj_type = document.createElement("INPUT");
obj_type.placeholder = "Object Type";
document.getElementById('create_objects').appendChild(obj_type);

var obj_posx= document.createElement("INPUT");
obj_posx.placeholder = "X:";
document.getElementById('create_objects').appendChild(obj_posx);

var obj_posy = document.createElement("INPUT");
obj_posy.placeholder = "Y:";
document.getElementById('create_objects').appendChild(obj_posy);

var obj_posz = document.createElement("INPUT");
obj_posz.placeholder = "Z:";
document.getElementById('create_objects').appendChild(obj_posz);

var newButton = document.createElement('input');
newButton.type = 'button';
newButton.value = 'create/update object';
newButton.onclick = function () 
{
	var event = {event_type: "create", name: obj_name.value, object_type: obj_type.value, position: new THREE.Vector3(obj_posx.value,obj_posy.value,obj_posz.value)};
	handle_event(event);
};
document.getElementById('create_objects').appendChild(newButton);


//Event for demo
var conveyer_event = {event_type: "create", name: "conveyer", object_type: "conveyer",position: new THREE.Vector3(-160,-100,0), rotationx: -Math.PI / 2};

var cap_event = {event_type: "create", name: "cap", object_type: "cap", position: new THREE.Vector3(-430,-160,0)};	
																												
var plane_event = 	 {event_type: "create", name: "plane", object_type: "plane"};																

var cap_arm_event = {event_type: "create", name: "cap_arm", object_type: "cap_arm",position: new THREE.Vector3(-330, -150, 0)};
																	//cap_arm.children[0].children[0].rotation.z = Math.PI / 10;"};																	
var arm_animate_event = {event_type:"update", name:"cap_arm"};

var cap_on_belt = {event_type:"update", name:"cap", time:'2000', delay:0, initPos:new THREE.Vector3(-250,-90,0), finalPos:new THREE.Vector3(-60,-90,0)};
				
var cap_left_of_arm = {event_type: "update", name: "cap", position: new THREE.Vector3(-430,-160,0)};	
				
//For setInterval events
var interval;
				
var newButton = document.createElement('input');
newButton.type = 'button';
newButton.value = 'intialise objects';
newButton.onclick = function () 
{
	/*for(var i = 0; i < events.length; i++)
	{
		handle_event(events[i]);
	}*/
	//console.log("Launch event");
	//console.log(event1.type);
	//handle_event(event2);
	//handle_event(event3);
	//update_event(event1);
	//handle_event(event4);
	handle_event(cap_event);
	//handle_event(cap_arm_event);
	//handle_event(plane_event);
	//handle_event(conveyer_event);
};
document.getElementById('header').appendChild(newButton);

var newButton = document.createElement('input');
newButton.type = 'button';
newButton.value = 'animate (simulate events)';
newButton.onclick = function () 
{
	handle_event(cap_left_of_arm);
	
	handle_event(arm_animate_event);
	
	setTimeout( function(){ handle_event(cap_on_belt);} , 2500);
	
};
document.getElementById('header').appendChild(newButton);

function handle_event(eventobj)
{
	console.log(eventobj.event_type + " event");
	if(eventobj.event_type == "create")
		create_event(eventobj);
	if(eventobj.event_type == "update")
		update_event(eventobj);
	if(eventobj.event_type == "delete")
		delete_event(eventobj);
	//Loop through received events
	//for(var i = 0; i < eventobj.length; i++)
	//run_event(eventobj[i]);
}


function create_event(eventobj)
{
	var obj;
	var name = eventobj.name;
	
	if(eventobj.object_type != null && eventobj.object_type != undefined )
	{
		if(functions[eventobj.object_type] != null && name != null)
		{
			if(name != null && name != undefined)
			{
				//Run fuction to create object
				functions[eventobj.object_type](name);
				
				obj = objects[name]["object"];
				
				objects[name]["type"] = eventobj.object_type;

				if(eventobj.rotationx != null && eventobj.rotationx != undefined)
				{
					obj.rotation.x = eventobj.rotationx;
				}
				if(eventobj.rotationy != null && eventobj.rotationy != undefined)
				{
					obj.rotation.y = eventobj.rotationy;
				}
				if(eventobj.rotationz != null && eventobj.rotationz != undefined)
				{
					obj.rotation.z = eventobj.rotationz;
				}
				if(eventobj.position != undefined && eventobj.position != undefined)
				{
					var pos = eventobj.position;
					//console.log(pos);
					obj.position.set(pos.x, pos.y, pos.z);
				}
			}
			else
			{
				console.log("Missing object name");
			}
	
		}
		else
		{
			console.log("Invalid or missing function")
		}

	}
	else
	{
		console.log("Create event received with no object type");
	}	
}

function update_event(eventobj)
{
	//console.log(name);
	
	var object = null;
	var stats = null;
	//Update rate in milliseconds
	//Approx 30 frames/sec (35*30 = 1050)
	var update_rate = 35;
	var time = 0;
	var name = eventobj.name;
	
	//search object array
	if(!(name in objects))
	{
		console.log("event received with non-existant object (name: " + name + " )");
		return;
	}
	else
	{
		//console.log("event running with object name: " + name);
		stats = objects[name];
		object = objects[name]["object"];
	}
	
	//First delay time
	setTimeout( function()
	{
		//Set initial position 
	
		if(eventobj.initPos == null)
		{
			eventobj.initPos = object.position;
		}
		if(eventobj.finalPos == null)
		{
			eventobj.finalPos = object.position;
		}
		
		//object.position.set(eventobj.initPos.x,eventobj.initPos.y,eventobj.initPos.z);
		
		//object.position.set(0,0,0);
	
	}, eventobj.delay);

	//Basic linear animation
	function animate()
	{
		var vec1 = new THREE.Vector3;
		vec1.copy(eventobj.initPos);
		vec1.multiplyScalar(1-(time/eventobj.time)); 
		var vec2 =  new THREE.Vector3;
		vec2.copy(eventobj.finalPos);
		vec2.multiplyScalar(time/eventobj.time);
		var vec3 = vec1.add(vec2);
		object.position.set(vec3.x, vec3.y, vec3.z);
		time+=update_rate;
		//console.log(time);
		//console.log(eventobj.time);
		//console.log(time/eventobj.time);
		//console.log(1-(time/eventobj.time));
		//console.log(eventobj.initPos.x);
		//console.log(vec1.x);
		
		if(time > eventobj.time)
		{
			console.log("animation end1");
			clearInterval(stats["interval"]);
		}
	}
	
	//Every update_rate milliseconds, play animate;
	//Position is for position jump - no animations
	if(eventobj.position != undefined)
	{
		var pos = eventobj.position;
		object.position.set(pos.x, pos.y, pos.z);
	}
	else if(stats["animation"] == undefined)
	{
		console.log(name + " no animation found");
		stats["interval"] = setInterval( function(){animate()}, update_rate);
	}
	else
	{
		console.log(name + " animation found");
		object["currAnimTime"] = 0;
		console.log(name + " animation time set to 0");
		stats["interval"] = setInterval(function(){stats["animation"](name, update_rate/1000)}, update_rate);
	}
}




var threexAR;
var markers	= {};
var cap = null;
var conveyer = null;
var cap_arm = null;
var plane = null;
var plane2 = null;
var cube = null;
var init_rotation = null;
var printed = false;
var printed2 = false;

var debug = false;

var init_position = new THREE.Vector3();
var init_quaternion = new THREE.Quaternion();
var init_scale = new THREE.Vector3();

var bottle_marker = 99;

window.onload	= function()
{
	var onCreate	= function(event){

		var markerId = event.markerId;
		var marker;
		console.assert(	markers[event.markerId] === undefined );
		markers[markerId] = {};
		marker = markers[markerId];
		//console.log("Marker" + markerId + "found");

		// create the container object
		marker.object3d = new THREE.Object3D();
		marker.object3d.matrixAutoUpdate = false;
		scene.add(marker.object3d);
		
		//For tip detections  - further code in update and delete
		if( markerId == bottle_marker )
		{
			if(cube == null)
			cube = add_cube(20,20,20);
			
			cube.textbox.style.visibility = "visible";
			marker.object3d.add(cube);
			
			//Save initial
			init_quaternion = get_quaternion(marker.object3d.matrixWorld);
			
			//1 radian = 57.2958 degrees
			printed = false;
			printed2 = false;
		}
	};
	var onDelete	= function(event){
		console.assert(	markers[event.markerId] !== undefined );
		var markerId	= event.markerId;
		var marker	= markers[markerId];
		//console.log("Marker" + markerId + "lost");
		
		
		//For tip detections  - further code in create and update
		if( markerId == bottle_marker )
		{
			if(cube == null)
			cube = add_cube();
			marker.object3d.add(cube);
			
			cube.textbox.innerHTML = "lost marker";
		}
		
		scene.remove( marker.object3d );
		//console.log("Length: " + objects.length);
		delete markers[markerId];
	};
	var onUpdate	= function(event)
	{
		
		console.assert(	markers[event.markerId] !== undefined );
		var markerId	= event.markerId;
		var marker	= markers[markerId];
		var obj_animations = new Array();
		marker.object3d.matrix.copy(event.matrix);
		marker.object3d.matrixWorldNeedsUpdate = true;	
		
		if( markerId == 22 )
		{
			//console.log("Marker" + markerId + "found");
			var children = marker.object3d.children;
	
			for (var key in objects) 
			{
				var found = false;
				
				for(var i = 0; i < children.length; i++ )
				{
					if(children[i] == objects[key])
					{
						found = true;
						//console.log("Found child " + objects[key].name + " on marker " + markerId);
						//console.log("Key = " + key);
						//objects[key].position.set(-60,-100,0);
						break;
					}
						
				}
				
				if(!found)
				{
					marker = markers[markerId];
					marker.object3d.add(objects[key]["object"]);
				}	
			}
		}
		

		
		//scene matrix world is the identity matrix, so we are setting the world matrix to identity
		//scene.updateMatrixWorld();
		
		//Loop through all of our objects. Anything done per object per tick done here;
		for (var key in objects) 
		{
			//Update the position of all the text boxes
			vector = toScreenPosition(objects[key]["object"], camera);
			objects[key]["textbox"].style.left = vector.x + 20;
			objects[key]["textbox"].style.top = vector.y + 20;
			
			//Update position of outline
			if(objects[key]["geometry"] != null && objects[key]["geometry"] != undefined)
			{
				object = objects[key]["object"];
				objects[key]["outline"].position.set(object.position.x, object.position.y, object.position.z);

			}
		}

		//For tip detections  - further code in create and delete
		if( markerId == bottle_marker )
		{
			var tip_threshold = 10;
			var quaternion = get_quaternion(marker.object3d.matrixWorld);

			if(	(quaternion.z - init_quaternion.z) * 57.2958 < -tip_threshold ||
				(quaternion.z - init_quaternion.z) * 57.2958 > tip_threshold  || 			
				(quaternion.x - init_quaternion.x) * 57.2958 < -tip_threshold ||
				(quaternion.x - init_quaternion.x) * 57.2958 > tip_threshold ||
				(quaternion.y - init_quaternion.y) * 57.2958 < -tip_threshold ||
				(quaternion.y - init_quaternion.y) * 57.2958 > tip_threshold)
			{
				/*console.log("x:" + (quaternion.x - init_quaternion.x) * 57.2958);
				console.log("y:" + (quaternion.y - init_quaternion.y) * 57.2958);
				console.log("z:" + (quaternion.z - init_quaternion.z) * 57.2958);*/
				cube.textbox.innerHTML = "tipping";
			}
			else
			{
				cube.textbox.innerHTML = "stable";
			}
		}
	};
	threexAR	= new THREEx.JSARToolKit({
		srcElement	: srcElement,
		threshold	: threshold,
		//canvasRasterW	: 640,
		//canvasRasterH	: 480,
		debug		: false,
		callback	: function(event){
			//console.log("event", event.type, event.markerId)
			if( event.type === 'create' ){
				onCreate(event);
			}else if( event.type === 'delete' ){
				onDelete(event);
			}else if( event.type === 'update' ){
				onUpdate(event);
			}else	console.assert(false, "invalid event.type "+event.type);
		}
	});

	// add threexAR.canvasRaster()) to the DOM
	threexAR.canvasRaster().id	= 'canvasRaster';
	//document.body.appendChild(threexAR.canvasRaster());	

	// start the animation
	animate();
};

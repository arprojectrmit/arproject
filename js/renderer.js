// var renderHeight = 900;
// var renderWidth = 1200;
var renderHeight = 0;
var renderWidth = 0;
// var renderHeight = 1080;
// var renderWidth = 1920;

//divs cannot be placed in three.js canvas renderer, so we generate a container that we can put them both in.
//Notice it is the same dimensions as the renderer.
var container = document.createElement('div');
container.style.height = renderHeight;					// THIS DOESN'T SOUND SAFE FOR MAINTAINING ASPECT RATIO.
container.style.width = renderWidth;
document.body.appendChild(container);

// setup three.js renderer
var renderer	= new THREE.WebGLRenderer({
	antialias	: true,
	preserveDrawingBuffer: true
});
renderer.domElement.id = 'renderer';
renderer.sortObjects = false;
renderer.setSize(renderWidth, renderHeight);			// NOW UNEEDED OR SHOULD IT HAVE SOME DEFAULT SETTING???
container.appendChild(renderer.domElement);

// create the scene
var scene	= new THREE.Scene();

var camera = new THREE.PerspectiveCamera(35, renderer.domElement.width/renderer.domElement.height, 1, 10000 );
camera.position.set(0, 0, 5);
scene.add(camera);

//////////////////////////////////////////////////////////////////////////////////
//										//
//////////////////////////////////////////////////////////////////////////////////

// create the video element for the webcam
var srcElement = document.createElement('video');
srcElement.width	= 120;
srcElement.height	= 240;
srcElement.loop	= true;
srcElement.volume	= 0;
srcElement.autoplay= true;
srcElement.controls= true;


var capabilities = {
	'mediaDevices' : false
}
// create the video element for the webcam
var threshold = 100;

document.querySelectorAll("#thresholdText")[0].innerHTML	= threshold;
document.querySelectorAll("#thresholdRange")[0].value		= threshold;

// Spits out console errors for any capabilities not supported.
check_capabilities(capabilities);

// Finds user camera and mic devices then displays video onto passed in
// video element. Comment out get_media_devices and comment in video from file
// to use video. NOTE : Might need to reopen tab after doing this. Browser
// sometimes seems to be insistent on using the previous used video source.
get_media_devices( srcElement );
// srcElement.src = "./video/move2.mp4";

// srcElement.addEventListener('playing', function() {
//     if (this.videoWidth === 0) {
//         console.error('videoWidth is 0. Camera not connected?');
//     } else {
//     	console.log(this.videoWidth);
//     	console.log(this.videoHeight);
//     }
// }, false);

var getVideoSize = function() {
	var screenWidth = $(window).width();
	var videoWidth = srcElement.videoWidth;
	var videoHeight = srcElement.videoHeight;
	var resizeVal = 0;
	

	if(screenWidth < videoWidth) {
		resizeVal = videoWidth / screenWidth;
		renderWidth = videoWidth / resizeVal;
		renderHeight = videoHeight / resizeVal;
	} else {
		renderWidth = videoWidth;
    	renderHeight = videoHeight;
	}
	
    renderer.setSize(renderWidth, renderHeight);

    srcElement.removeEventListener('playing', getVideoSize, false);
};

window.addEventListener("resize", function() {
	
     getVideoSize();
});

srcElement.addEventListener('playing', getVideoSize, false);

// console.log(srcElement.videoWidth);


//////////////////////////////////////////////////////////////////////////////////
//							Phil's Functions									//
//////////////////////////////////////////////////////////////////////////////////

function check_capabilities( capabilites ) {
	if (!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) {
  		console.log("enumerateDevices() not supported.");
  		capabilites[ 'mediaDevices' ] = false;
	} else {
		capabilites[ 'mediaDevices' ] = true;
	}
}

// Finds available microphones and cameras on the users device. Sets camera to display
// on provided video element.
function get_media_devices( video_el ) {

	// If mediaDevices API avalaible use it otherwise use MediaStreamTrack (depreciated).
	if (capabilities['mediaDevices']) {
		navigator.mediaDevices.enumerateDevices()
		.then(function(devices) {
			var audioSource = null;
		  	var videoSource = null;

		  	for (var i = 0; i != devices.length; ++i) {
		  
		    	var sourceInfo = devices[i];
		    	if (sourceInfo.kind === 'audioinput') {
			      	// console.log(sourceInfo.deviceId, sourceInfo.label || 'microphone');
			      	audioSource = sourceInfo.deviceId;
		    	} else if (sourceInfo.kind === 'videoinput') {
		      		// console.log(sourceInfo.deviceId, sourceInfo.label || 'camera');
				    videoSource = sourceInfo.deviceId;
		    	} else {
		      		// console.log('Some other kind of source: ', sourceInfo);
		    	}
		  	}

		  	set_video_stream(audioSource, videoSource, video_el);

		})
		.catch(function(err) {
		  	console.log(err.name + ": " + err.message);
		});
	} else {
		MediaStreamTrack.getSources(function(sourceInfos) {
		  	var audioSource = null;
		  	var videoSource = null;

		  	for (var i = 0; i != sourceInfos.length; ++i) {
		    	var sourceInfo = sourceInfos[i];
		    	if (sourceInfo.kind === 'audio') {
			      	// console.log(sourceInfo.id, sourceInfo.label || 'microphone');
			      	audioSource = sourceInfo.id;
		    	} else if (sourceInfo.kind === 'video') {
		      		// console.log(sourceInfo.id, sourceInfo.label || 'camera');
				    videoSource = sourceInfo.id;
		    	} else {
		      		// console.log('Some other kind of source: ', sourceInfo);
		    	}
		  	}
		  	set_video_stream(audioSource, videoSource, video_el);
		});
	}
}


function set_video_stream(audioSource, videoSource, video_el) {
  	var constraints = {
    	audio: {
     		// optional: [{sourceId: audioSource}]
    	},
    	video: {
      		optional: [{sourceId: videoSource}],
      		// facingMode: { exact: "environment" }
    	}
	};

	var getUserMedia = function(t, success, error) {
		if (navigator.getUserMedia) { 									// WebRTC 1.0 standard compliant browser
		  	return navigator.getUserMedia(t, success, error);
		} else if (navigator.webkitGetUserMedia) {						// early webkit webrtc implementation
		  	return navigator.webkitGetUserMedia(t, success, error);
		} else if (navigator.mozGetUserMedia) {							// early firefox webrtc implementation
		    return navigator.mozGetUserMedia(t, success, error);
		} else if (navigator.msGetUserMedia) {
		    return navigator.msGetUserMedia(t, success, error);
		} else {
		    error(new Error("No getUserMedia implementation found."));
		}
	};

	getUserMedia(constraints,
		function(stream) {
		    if (window.URL) {
		    	window.URL = window.URL || window.webkitURL || window.mozURL || window.msURL;
			    video_el.src = window.URL.createObjectURL(stream) || stream;
			    video_el.play();
			} else {
			    video_el.src = stream || stream; // Opera.
			    video_el.play();
			}

			video_el.onerror = function(e) {
				stream.stop();
			};

			stream.onended = noStream;
		},  
		function(error) {
			alert("Couldn't access webcam.");
		}
	);
}

function noStream(e) {
	var msg = 'No camera available.';
	if (e.code == 1) {
	   	msg = 'User denied access to use camera.';
	}
	alert(msg);
	// document.getElementById('errorMessage').textContent = msg;
}

//////////////////////////////////////////////////////////////////////////////////
//										//
//////////////////////////////////////////////////////////////////////////////////

// TODO global to remove
var videoTex;
var videoCam, 	videoScene;

// Create scene and quad for the video.
videoTex 	= new THREE.Texture(srcElement);
videoTex.minFilter = THREE.LinearFilter;
var geometry	= new THREE.PlaneGeometry(2, 2, 0);
var material	= new THREE.MeshBasicMaterial({
	map		: videoTex,
	depthTest	: false,
	depthWrite	: false
});
var plane	= new THREE.Mesh(geometry, material );
videoScene	= new THREE.Scene();
videoCam	= new THREE.Camera();
videoScene.add(plane);
videoScene.add(videoCam);


//////////////////////////////////////////////////////////////////////////////////
//										//
//////////////////////////////////////////////////////////////////////////////////


function animate()
{	
	requestAnimationFrame(animate);
	render();
};

function render()
{
	if( true )
	{
		if( srcElement instanceof HTMLImageElement )
		{
			videoTex.needsUpdate	= true;
			threexAR.update();
		}
		else if( srcElement instanceof HTMLVideoElement && srcElement.readyState === srcElement.HAVE_ENOUGH_DATA )
		{
			videoTex.needsUpdate	= true;
			threexAR.update();
		}		
	}
	// trigger the rendering
	renderer.autoClear = false;
	renderer.clear();
	renderer.render(videoScene, videoCam);
	renderer.render(scene, camera);
};

//////////////////////////////////////////////////////////////////////////////////
//										//
//////////////////////////////////////////////////////////////////////////////////

var no_videos = 4;
var x = 1;
var button_ids = new Array();
var button_directories = new Array();
var directories = new Array();
var threshold = new Array();

directories.push("./video/level.mp4");
threshold.push(65);
directories.push("./video/overhead.mp4");
threshold.push(65);
directories.push("./video/move2.mp4");
threshold.push(100);
directories.push("./video/still.mp4");
threshold.push(100);



for(var i = 0; i < directories.length; i++)
{
	var newButton = document.createElement('input');
	newButton.type = 'button';
	newButton.value = 'video' + i;
	newButton.id = 'button' + i;
	//console.log(directories[i]);
	
	newButton.setAttribute("onclick", 'srcElement.src = directories[' + i + '];' + 
																'setThreshold(threshold[' + i + ']); + srcElement.addEventListener(\'playing\', getVideoSize, false);');
	
	document.getElementById('video_sources').appendChild(newButton);
}

var newButton = document.createElement('input');
newButton.type = 'button';
newButton.value = 'webcam';
newButton.setAttribute("onclick", 'get_media_devices( srcElement ); srcElement.addEventListener(\'playing\', getVideoSize, false);');
document.getElementById('video_sources').appendChild(newButton);



	

